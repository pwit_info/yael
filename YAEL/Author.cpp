#include "stdafx.h"
#include "Author.h"
#include <tuple>

string Author::getName() const
{
	return name;
}

string Author::getDescription() const
{
	return description;
}

Author::Author(const string& name, const string& description): name(name),
                                                               description(description)
{
}


bool TeamOfAuthors::subsetOf(const TeamOfAuthors & team_b) const
{

	for(const Author& a: team)
	{
		if (!team_b.has(a))
			return false;
	}
	return true;
}

TeamOfAuthors::TeamOfAuthors(Author author)
{
	team.push_front(author);
}

void TeamOfAuthors::add(const Author& am)
{
	team.push_front(am);
}

bool TeamOfAuthors::has(const Author & am) const
{ 

	return (find(team.begin(), team.end(), am) != team.end());
}

void TeamOfAuthors::remove(const Author& am)
{
	team.remove(am);
}

bool operator==(const Author& lhs, const Author& rhs)
{

	return tie(lhs.name, lhs.description) == tie(rhs.name, rhs.description);
}

bool operator!=(const Author& lhs, const Author& rhs)
{
	return !(lhs == rhs);
}

bool operator==(const TeamOfAuthors& lhs, const TeamOfAuthors& rhs)
{
	return lhs.subsetOf(rhs) && rhs.subsetOf(lhs);
}

bool operator!=(const TeamOfAuthors& lhs, const TeamOfAuthors& rhs)
{
	return !(lhs == rhs);
}
