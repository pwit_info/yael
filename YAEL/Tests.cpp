#include "stdafx.h"

#include "gtest/gtest.h"
#include "Ebook.h"
#include "Author.h"
#include "File.h"
#include "Backend.h"

TEST(AuthorTest, Creation)
{
	ASSERT_NO_THROW(Author AdamMickiewicz("Adam Mickiewicz", "Wielki polski poeta romantyczny. �y� w latach 1798 - 1855."));

}


Author AdamMickiewicz("Adam Mickiewicz", "Wielki polski poeta romantyczny. �y� w latach 1798 - 1855.");
Author JuliuszSlowacki("Juliusz S�owacki", "Wielki polski poeta romantyczny. �y� w latach  1809 - 1849.");


TEST(AuthorTest, Equalities)
{
	ASSERT_TRUE(AdamMickiewicz == AdamMickiewicz);
	ASSERT_FALSE(AdamMickiewicz != AdamMickiewicz);

	ASSERT_TRUE(AdamMickiewicz != JuliuszSlowacki);
	ASSERT_FALSE(AdamMickiewicz == JuliuszSlowacki);
}


TEST(TeamOfAuthorsTest, Creation)
{
	ASSERT_NO_THROW(TeamOfAuthors t);
	ASSERT_NO_THROW(TeamOfAuthors tAM(AdamMickiewicz));
}


TEST(TeamOfAuthorsTest, SimpleManipuations)
{
	TeamOfAuthors team;
	list<Author> authors;

	ASSERT_FALSE(team.has(AdamMickiewicz));
	ASSERT_EQ(team.asList(), authors);

	team.add(AdamMickiewicz);
	authors.push_front(AdamMickiewicz);

	ASSERT_TRUE(team.has(AdamMickiewicz));
	ASSERT_EQ(team.asList(), authors);
	ASSERT_FALSE(team.has(JuliuszSlowacki));

	team.add(JuliuszSlowacki);
	authors.push_front(JuliuszSlowacki);

	ASSERT_TRUE(team.has(JuliuszSlowacki));
	ASSERT_TRUE(team.has(AdamMickiewicz));
	ASSERT_EQ(team.asList(), authors);
	
	team.remove(AdamMickiewicz);
	authors.remove(AdamMickiewicz);

	ASSERT_FALSE(team.has(AdamMickiewicz));
	ASSERT_TRUE(team.has(JuliuszSlowacki));
	ASSERT_EQ(team.asList(), authors);

}


TEST(TeamOfAuthorsTest, Equalities)
{
	TeamOfAuthors teamA, teamB;

	ASSERT_TRUE(teamA == teamB);
	ASSERT_FALSE(teamA != teamB);

	teamA.add(AdamMickiewicz);

	ASSERT_FALSE(teamA == teamB);
	ASSERT_TRUE(teamA != teamB);

	teamB.add(JuliuszSlowacki);

	ASSERT_FALSE(teamA == teamB);
	ASSERT_TRUE(teamA != teamB);

	teamA.remove(AdamMickiewicz);
	teamB.remove(JuliuszSlowacki);

	ASSERT_TRUE(teamA == teamB);
	ASSERT_FALSE(teamA != teamB);

	teamA.add(AdamMickiewicz);
	teamB.add(AdamMickiewicz);

	ASSERT_TRUE(teamA == teamB);
	ASSERT_FALSE(teamA != teamB);

	teamA.add(JuliuszSlowacki);
	teamB.add(JuliuszSlowacki);

	ASSERT_TRUE(teamA == teamB);
	ASSERT_FALSE(teamA != teamB);
}


TEST(TeamOfAuthorsTest, PermutationsOfAuthorsShouldBeEqual)
{
	TeamOfAuthors teamA, teamB;

	teamA.add(JuliuszSlowacki);
	teamA.add(AdamMickiewicz);

	teamB.add(AdamMickiewicz);
	teamB.add(JuliuszSlowacki);

	ASSERT_TRUE(teamA == teamB);
	ASSERT_FALSE(teamA != teamB);


}


TEST(EbookTests, Creation)
{
	ASSERT_NO_THROW(Ebook e("Pan Tadeusz", AdamMickiewicz, "Wydanie MCII", "PWN", "12345", SetOfFiles()));
}

Ebook PanTadeusz("Pan Tadeusz", AdamMickiewicz, "Wydanie MCII", "PWN", "12345", SetOfFiles());
Ebook Balladyna("Balladyna", JuliuszSlowacki, "Wydanie CXX", "Wydawnictwo Literackie", "23456", SetOfFiles());

TEST(EbookTests, Equalities)
{
	ASSERT_TRUE(PanTadeusz == PanTadeusz);
	ASSERT_FALSE(PanTadeusz != PanTadeusz);

	ASSERT_TRUE(PanTadeusz != Balladyna);
	ASSERT_FALSE(PanTadeusz == Balladyna);

}

TEST(EbookTests, GetSetTests)
{
	Ebook kniga = PanTadeusz;

	ASSERT_EQ(kniga.getTitle(), "Pan Tadeusz");
	ASSERT_EQ(kniga.getAuthors(), AdamMickiewicz);
	ASSERT_EQ(kniga.getEdition(), "Wydanie MCII");
	ASSERT_EQ(kniga.getPublisher(), "PWN");
	ASSERT_EQ(kniga.getIsbn(), "12345");

   
	kniga.setTitle("Balladyna");
	kniga.setAuthors(JuliuszSlowacki);
	kniga.setEdition("Wydanie CXX");
	kniga.setPublisher("Wydawnictwo Literackie");
	kniga.setIsbn("23456");

	ASSERT_EQ(kniga, Balladyna);

	//TODO: test getters/setters for kniga.files field.
}



TEST(FileTest, Creation)
{
	vector<char> file_content;

	ASSERT_NO_THROW(File f("KRANSIC.pdf", "PDF, OCR", file_content));

}

TEST(FileTest, GettersTests)
{
	vector<char> file_content;

	File f("KRANSIC.pdf", "PDF, OCR", file_content);

	ASSERT_EQ(f.getName(), "KRANSIC.pdf");
	ASSERT_EQ(f.getDescription(), "PDF, OCR");
	ASSERT_NE(f.getDescription(), "PDD, OCR");
	ASSERT_EQ(f.getContent(), file_content);
}


TEST(SetOfFilesTest, Creation)
{
	ASSERT_NO_THROW(SetOfFiles sof);


}

TEST(SetOfFilesTest, SimpleManipulations)
{

	vector<char> file_content;
	File f("KRANSIC.pdf", "PDF, OCR", file_content);
	SetOfFiles sof;
	ASSERT_FALSE(sof.has(f));
	sof.add(f);
	ASSERT_TRUE(sof.has(f));
	sof.remove(f);
	ASSERT_FALSE(sof.has(f));
}





TEST(BackendTest, BackendCreation)
{

	ASSERT_NO_THROW(InMemoryBackend b);
}

TEST(BackendTest, BackendAuthorsManipulation)
{
	InMemoryBackend b;

	ASSERT_EQ(b.getAuthors(), list<Author>());

	b.addAuthor(AdamMickiewicz);
	list<Author> allAuthors;
	allAuthors.push_back(AdamMickiewicz);

	ASSERT_EQ(b.getAuthors(), allAuthors);

	b.removeAuthor(AdamMickiewicz);

	ASSERT_EQ(b.getAuthors(), list<Author>());


}

TEST(BackendTest, BackendEbookManipulation)
{
	
	InMemoryBackend b;
	list<Ebook> l;

	ASSERT_EQ(b.getAllEbooks(), l);
	b.addAuthor(AdamMickiewicz);
	Ebook PanTadeusz("Pan Tadeusz", AdamMickiewicz, "Wydanie XX", "PWN", "12345", SetOfFiles());
	ASSERT_NO_THROW(b.addEbook(PanTadeusz));
	l.push_front(PanTadeusz);
	ASSERT_EQ(b.getAllEbooks(), l);
	l.remove(PanTadeusz);
	b.removeEbook(PanTadeusz);
	ASSERT_EQ(b.getAllEbooks(), l);
	b.addEbook(PanTadeusz);
//	PanTadeusz.updateTitle


//	l.push_front(AdamMickiewicz);
//	ASSERT_EQ(b.getAllEbooks(), l);
//	l.remove(AdamMickiewicz);
//	b.removeAuthor(AdamMickiewicz);
//	ASSERT_EQ(b.getAllEbooks(), l);

}


TEST(BackendTest, EbookInsertionFailsWhenAuthorNotInBase)
{
	InMemoryBackend b;
	Ebook PanTadeusz("Pan Tadeusz", AdamMickiewicz, "Wydanie XX", "PWN", "12345", SetOfFiles());
	ASSERT_THROW(b.addEbook(PanTadeusz), AuthorNotInDatabase);
}

TEST(BackendTest, EbookInsertionSucceedsWhenAuthorInBase)
{
	InMemoryBackend b;
	Ebook PanTadeusz("Pan Tadeusz", AdamMickiewicz, "Wydanie XX", "PWN", "12345", SetOfFiles());
	b.addAuthor(AdamMickiewicz);
	ASSERT_NO_THROW(b.addEbook(PanTadeusz));
}

TEST(BackendTest, EbookInsertionSucceedsWhenEbookNotInBase)
{
	InMemoryBackend b;
	Ebook PanTadeusz("Pan Tadeusz", AdamMickiewicz, "Wydanie XX", "PWN", "12345", SetOfFiles());
	b.addAuthor(AdamMickiewicz);
	ASSERT_NO_THROW(b.addEbook(PanTadeusz));

}

TEST(BackendTest, EbookInsertionFailsWhenEbookAlreadyInBase)
{
	InMemoryBackend b;
	Ebook PanTadeusz("Pan Tadeusz", AdamMickiewicz, "Wydanie XX", "PWN", "12345", SetOfFiles());
	b.addAuthor(AdamMickiewicz);
	b.addEbook(PanTadeusz);

	ASSERT_THROW(b.addEbook(PanTadeusz), EbookAlreadyInDatabase);

}

TEST(BackendTest, EbookUpdate)
{
	InMemoryBackend b;
	b.addAuthor(AdamMickiewicz);
	b.addEbook(PanTadeusz);
   
	Ebook kopia = PanTadeusz;
	kopia.setEdition("Wydanie CCC");

	ASSERT_NO_THROW(b.updateEbook(PanTadeusz, kopia));
	ASSERT_THROW(b.updateEbook(PanTadeusz, kopia), EbookNotInDatabase);

	b.addAuthor(JuliuszSlowacki);
	b.addEbook(Balladyna);

	ASSERT_THROW(b.updateEbook(kopia, Balladyna), EbookAlreadyInDatabase);
}





