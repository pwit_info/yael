﻿#include "stdafx.h"
#include "Ebook.h"

string Ebook::getTitle() const
{
	return title;
}

void Ebook::setTitle(const string& title)
{
	this->title = title;
}

TeamOfAuthors Ebook::getAuthors() const
{
	return authors;
}

void Ebook::setAuthors(const TeamOfAuthors& authors)
{
	this->authors = authors;
}

string Ebook::getPublisher() const
{
	return publisher;
}

void Ebook::setPublisher(const string& publisher)
{
	this->publisher = publisher;
}

string Ebook::getIsbn() const
{
	return isbn;
}

void Ebook::setIsbn(const string& isbn)
{
	this->isbn = isbn;
}

SetOfFiles Ebook::getFiles() const
{
	return files;
}

void Ebook::setFiles(const SetOfFiles& files)
{
	this->files = files;
}

string Ebook::getEdition() const
{
	return edition;
}

void Ebook::setEdition(const string& edition)
{
	this->edition = edition;
}

list<Author> Ebook::getListOfAuthors() const
{
	return authors.asList();
}

Ebook::Ebook(const string& title, const TeamOfAuthors& authors, const string& edition,
	const string& publisher, const string& isbn, SetOfFiles files) : title(title),
	authors(authors),
	edition(edition),
	publisher(publisher),
	isbn(isbn),
	files(files)
{
	
}

bool operator==(const Ebook& lhs, const Ebook& rhs)
{
	return lhs.title == rhs.title
		&& lhs.authors == rhs.authors
		&& lhs.edition == rhs.edition
		&& lhs.publisher == rhs.publisher
		&& lhs.isbn == rhs.isbn
		&& lhs.files == rhs.files;
}

bool operator!=(const Ebook& lhs, const Ebook& rhs)
{
	return !(lhs == rhs);
}
