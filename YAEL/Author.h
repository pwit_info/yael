#pragma once
#include <list>

using namespace std;

class Author {
public:
	string getName() const;
	string getDescription() const;
private:
	friend bool operator==(const Author& lhs, const Author& rhs);
	friend bool operator!=(const Author& lhs, const Author& rhs);

public:
	Author(const string& name, const string& description);

private:
	string name;
	string description;

};


class TeamOfAuthors
{
	friend bool operator==(const TeamOfAuthors& lhs, const TeamOfAuthors& rhs);
	friend bool operator!=(const TeamOfAuthors& lhs, const TeamOfAuthors& rhs);


	list<Author> team;

	bool subsetOf(const TeamOfAuthors& team_b) const;

public:

	TeamOfAuthors() {}
	TeamOfAuthors(Author author);

	void add(const Author& am);
	bool has(const Author& am) const;
	void remove(const Author& am);
	list<Author> asList() const { return team; }
};

