#pragma once
#include "Author.h"
#include "Ebook.h"
#include <list>


class Backend {

public:
	virtual list<Author> getAuthors() const = 0;
	virtual void addAuthor(const Author& am) = 0;
	virtual void removeAuthor(const Author& am) = 0;
	virtual bool hasAuthor(Author author);

	virtual list<Ebook> getAllEbooks() const = 0;
	virtual void addEbook(const Ebook& ebook) = 0;
	virtual void removeEbook(const Ebook& ebook) = 0;
	virtual bool hasEbook(const Ebook& ebook);
	virtual void updateEbook(const Ebook& oldEbook, const Ebook& newEbook) = 0;


	virtual ~Backend() = 0;


};




class InMemoryBackend: public Backend {

	list<Author> allAuthors;
	list<Ebook> allEbooks;

	/*
	* list<Author> getAuthors();
	* addAuthor(author);
	* removeAutor(auhtor);
	*
	* list<Ebook> getAllEbooks();
	* list<Ebook> getMatchingEbooks(Pattern p);
	* removeEbook(Ebook e);
	* addEbook(Ebook e);
	* updateEbook(Ebook old, Ebook new);
	*/

public:
	list<Author> getAuthors() const override;
	void addAuthor(const Author& am) override;
	void removeAuthor(const Author& am) override;

	list<Ebook> getAllEbooks() const override;
	void addEbook(const Ebook& ebook) override;
	void removeEbook(const Ebook& ebook) override;
	void updateEbook(const Ebook& oldEbook, const Ebook& newEbook) override;
};




class BackendException{};

class AuthorNotInDatabase: public BackendException{};
class EbookAlreadyInDatabase: public BackendException{};
class EbookNotInDatabase: public BackendException{};