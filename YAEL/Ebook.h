﻿#pragma once

#include "stdafx.h"
#include "Author.h"
#include "File.h"

using namespace std;

class Ebook
{
public:
	string getTitle() const;
	void setTitle(const string& title);
	TeamOfAuthors getAuthors() const;
	void setAuthors(const TeamOfAuthors& authors);
	string getPublisher() const;
	void setPublisher(const string& publisher);
	string getIsbn() const;
	void setIsbn(const string& isbn);
	SetOfFiles getFiles() const;
	void setFiles(const SetOfFiles& files);

	string getEdition() const;

	void setEdition(const string& edition);

	list<Author> getListOfAuthors() const;


private:
	friend bool operator==(const Ebook& lhs, const Ebook& rhs);
	friend bool operator!=(const Ebook& lhs, const Ebook& rhs);

public:
	Ebook(const string& title, const TeamOfAuthors& authors, const string& edition, const string& publisher, const string& isbn, SetOfFiles files);

private:
	string title;
	TeamOfAuthors authors;
	string edition;
	string publisher;
	string isbn;
	SetOfFiles files;
};
