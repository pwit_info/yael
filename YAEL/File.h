#pragma once
#include <list>
#include <vector>

using namespace std;

class File {
public:
	string getDescription() const;
	string getName() const;
	vector<char> getContent() const;
private:
	friend bool operator==(const File& lhs, const File& rhs);

	friend bool operator!=(const File& lhs, const File& rhs);

public:
	File(const string& name, const string& description, vector<char> file_bytes);

private:
	string name;
	string description;
	vector<char> file_contents;


};

class SetOfFiles {
	friend bool operator==(const SetOfFiles& lhs, const SetOfFiles& rhs);

	friend bool operator!=(const SetOfFiles& lhs, const SetOfFiles& rhs);

	list<File> set;

public:
	void add(const File& file);

	void remove(const File& file);
	bool has(const File& file);
};
