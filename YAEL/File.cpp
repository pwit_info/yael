#include "stdafx.h"
#include "File.h"

string File::getDescription() const
{
	return description;
}

string File::getName() const
{
	return name;
}

vector<char> File::getContent() const
{
	return file_contents;
}

File::File(const string& name, const string& description, vector<char> file_bytes): name(name),
                                                                             description(description),
                                                                             file_contents(file_bytes)
{
}

void SetOfFiles::add(const File& file)
{
	set.push_front(file);
}

void SetOfFiles::remove(const File& file)
{
	set.remove(file);
}

bool SetOfFiles::has(const File& file)
{
	return (find(set.begin(), set.end(), file) != set.end());
}

bool operator==(const File& lhs, const File& rhs)
{
	return lhs.name == rhs.name
		&& lhs.description == rhs.description
		&& lhs.file_contents == rhs.file_contents;
}

bool operator!=(const File& lhs, const File& rhs)
{
	return !(lhs == rhs);
}

bool operator==(const SetOfFiles& lhs, const SetOfFiles& rhs)
{
	return lhs.set == rhs.set;
}

bool operator!=(const SetOfFiles& lhs, const SetOfFiles& rhs)
{
	return !(lhs == rhs);
}
