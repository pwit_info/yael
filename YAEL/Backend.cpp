#include "stdafx.h"
#include "Backend.h"

bool Backend::hasAuthor(Author author)
{
	list<Author> allAuthors = getAuthors();
	return (find(allAuthors.begin(), allAuthors.end(), author) != allAuthors.end());
}

bool Backend::hasEbook(const Ebook& ebook)
{
	list<Ebook> allEbooks = getAllEbooks();
	return (find(allEbooks.begin(), allEbooks.end(), ebook) != allEbooks.end());
}

list<Author> InMemoryBackend::getAuthors() const
{
	return allAuthors; 

}

void InMemoryBackend::addAuthor(const Author& am)
{
	allAuthors.push_front(am);
}

void InMemoryBackend::removeAuthor(const Author& am)
{
	allAuthors.remove(am);
}


list<Ebook> InMemoryBackend::getAllEbooks() const
{
	return allEbooks;
}

void InMemoryBackend::addEbook(const Ebook& ebook)
{

	if (this->hasEbook(ebook))
		throw EbookAlreadyInDatabase();

	for (auto & author : ebook.getListOfAuthors())
	{
		if (!(this->hasAuthor(author)))
			throw AuthorNotInDatabase();
	}

	allEbooks.push_front(ebook);
}

void InMemoryBackend::removeEbook(const Ebook& ebook)
{
	allEbooks.remove(ebook);
}


void InMemoryBackend::updateEbook(const Ebook & oldEbook, const Ebook & newEbook)
{
	if ( !(this->hasEbook(oldEbook))) throw EbookNotInDatabase();

	removeEbook(oldEbook);
	addEbook(newEbook);


}



